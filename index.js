'use strict';

var path = require('path'),
    fs = require('fs'),
    gutil = require('gulp-util'),    
    through = require('through2'),
    extend = require('extend'),
    propsParser = require('properties-parser'),
    BufferStreams = require('bufferstreams'),
    PluginError = gutil.PluginError,
    PLUGIN_NAME = 'gulp-locale';

function props2json(filePath, buffer, options) {    
    
    var props, 
        output, 
        fileExt,
        fileName,
        localeDelimiterIndex,
        localeGroupName,
        localeCode,
        endOfLineChar;
    
    props = propsParser.parse(buffer.toString('utf8'));
    fileExt = path.extname(filePath);
    fileName = path.basename(filePath, fileExt);
    localeDelimiterIndex = fileName.lastIndexOf('_');
    output = [];
    localeGroupName = fileName.substring(0, localeDelimiterIndex); 
    localeCode = fileName.substring(localeDelimiterIndex + 1);    

    output.push("locale.add('" + options.namespace + "', '" + localeCode + "', '" + localeGroupName + "', {");
    
    Object.keys(props).forEach(function(key, i, arr) {
        endOfLineChar = i < arr.length - 1 ? ',' : '';
        output.push("    " + key + ": '" + props[key].replace(/'/g, '\'') + "'" + endOfLineChar);
    });    
    
    output.push('});\n');  
    
    return new Buffer(output.join('\n'));
}


function file(options) {
    options = extend({ namespace: 'config', space: null, replacer: null }, options);

    return through.obj(function(file, enc, callback) {
        if (file.isStream()) {
            file.contents = file.contents.pipe(new BufferStreams(function(err, buf, cb) {
                if (err) {
                    this.emit('error', new PluginError(PLUGIN_NAME, err.message));
                }
                else {
                    try {
                        cb(null, props2json(buf, options));
                        file.contents = props2json(file.contents, options);
                        file.path = gutil.replaceExtension(file.path, options.namespace ? '.js' : '.json');
                    }
                    catch (error) {
                        this.emit('error', new PluginError(PLUGIN_NAME, error.message));
                        cb(error);
                    }
                }
            }));
        }
        else if (file.isBuffer()) {
            try {
                file.contents = props2json(file.path, file.contents, options);
                file.path = gutil.replaceExtension(file.path, options.namespace ? '.js' : '.json');
            }
            catch (error) {
                this.emit('error', new PluginError(PLUGIN_NAME, error.message));
                return callback();
            }
        }
        
        this.push(file);
        return callback();
    });
};


function header(options) {
    var headerContent = fs.readFileSync(path.join(__dirname, 'locale_header.js'), options) + '\n\n';

    return through.obj(function(file, enc, callback) {
        if (file.isBuffer()) {
            file.contents = Buffer.concat([
                new Buffer(headerContent),
                file.contents
            ]);
        }
        else if (file.isStream()) {
            var stream = through();
            stream.write(new Buffer(headerContent));
            stream.on('error', this.emit.bind(this, 'error'));
            file.contents = file.contents.pipe(stream);
        }
        
        this.push(file);
        return callback();
    });
};

function wrapper(defaultLocaleCode) {
    
    var headerContent = fs.readFileSync(path.join(__dirname, 'locale_header.js'), {encoding: 'utf8'});
    var footerContent = fs.readFileSync(path.join(__dirname, 'locale_footer.js'), {encoding: 'utf8'});
    
    footerContent = footerContent.replace('{{defaultLocaleCode}}', defaultLocaleCode);
    
    return {
        header: headerContent + '\n\n',
        footer: '\n\n' + footerContent + '\n\n'
    };    
}


module.exports = {
    file: file,
    wrapper: wrapper
};