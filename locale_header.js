
/**
 * Dependencies: lodash (_) 
 */
!function () {
    
    'user strict';
    
    var localeScope, 
        defaultLocale,
        namespaces = {};
    
//    function merge(d,a){var c;for(c in d){if(d.hasOwnProperty(c)){a[c]=d[c];}}return a;}
//    function ns(b,c){var e=c.split('.');var d=e.length;var a=0;while(a<d){b[e[a]]=b[e[a]]||{};b=b[e[a]];a++;}return b;}
//    function express(e,t){var n=t.split('.'),r=e,i=n.length,s=0;while(s<i){r=r[n[s++]]}return r;}
      
    function ns(scope, parts, value) {        
        parts = parts.split('.');        
        var partLength = parts.length, 
            obj = scope, 
            i = 0;                
        while (i < partLength) {
            if (i === partLength - 1) {
                obj = obj[parts[i]] = (typeof value !== 'undefined') ? value : {};
            }
            else {
                obj = obj[parts[i]] = obj[parts[i]] || {};
            }
            i++;
        }        
        return obj;
    }
    
    function add(namespace, localeCode, localeGroup, props) {    
        
       
        namespaces[namespace] = namespaces[namespace] || {};
        namespaces[namespace][localeCode] = namespaces[namespace][localeCode] || {};
         // namespaces['Thunderhead.layoutEditor']['en']
        ns(namespaces[namespace][localeCode], localeGroup, props); // localeGroup could be a path (e.g. components.buttons)
        
         // namespaces['Thunderhead.layoutEditor']['fr-FR']['components']['buttons'] = props
    }
    
    function createFallbacks(defaultLocaleCode) {
        var parts;
        _.forIn(namespaces, function(namespaceValue, namespaceName) {            
            // namespaceName = Thunderhead.layoutEditor                       
            _.forIn(namespaceValue, function(localeCodeValue, localeCode) {   
                if (localeCode !== defaultLocaleCode) {
                    parts = localeCode.split('-');
                    if (parts.length === 2 && parts[0] !== defaultLocaleCode) {                    
                        // localeCode = fr-FR
                        _.merge(localeCodeValue, namespaces[namespaceName][parts[0]], function(objectValue, sourceValue) {
                            if (typeof sourceValue === 'string') {                                
                                return typeof objectValue === 'undefined' ? sourceValue : objectValue;
                            }
                        });               
                    }
                    //else localeCode = fr
                    _.merge(localeCodeValue, namespaces[namespaceName][defaultLocaleCode], function(objectValue, sourceValue) {
                        if (typeof sourceValue === 'string') {                                
                            return typeof objectValue === 'undefined' ? sourceValue : objectValue;
                        }
                    });
                }
            });            
        });
    }

    function init(scope, defaultLocaleCode) {
        localeScope = scope;
        defaultLocale = defaultLocaleCode;
        createFallbacks(defaultLocaleCode);
        
        this.use(defaultLocaleCode);
    }
    
    function use(localeCode) {
        var locale, parts = localeCode.split('-'); // localeCode = es-
        _.forIn(namespaces, function(value, key) { 
            // key 'Thunderhead.layoutEditor'
            // locale = namespaces['Thunderhead.layoutEditor']['fr-FR'] || namespaces['Thunderhead.layoutEditor']['fr'] || namespaces['Thunderhead.layoutEditor']['en']
            locale = value[localeCode] || value[parts[0]] || value[defaultLocale];
            ns(localeScope, key + '.locale', locale);    
        });
    }
    
    function log() {
        console.log(namespaces);        
    }
    
    // >>> temporary code
    this.th_locale_merge = function(d,a){var c;for(c in d){if(d.hasOwnProperty(c)){a[c]=d[c];}}return a;};
    this.th_locale_ns = function (c){var e=c.split('.');var b=window;var d=e.length;var a=0;while(a<d){b[e[a]]=b[e[a]]||{};b=b[e[a]];a++;}};
    // <<<

    this.locale = {
        add: add,
        init: init,
        use: use,
        log: log
    };
    
}();